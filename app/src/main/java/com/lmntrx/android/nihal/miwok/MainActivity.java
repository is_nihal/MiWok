package com.lmntrx.android.nihal.miwok;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void goToNextActivity(View view)
    {
        TextView textView=(TextView)view;
        int id=textView.getId();
            if(id==R.id.phrasesText) {
                Intent i = new Intent(getApplicationContext(), Phrases.class);
                startActivity(i);
            }
        if(id==R.id.colorsText) {
            Intent i = new Intent(getApplicationContext(), Colors.class);
            startActivity(i);
        }
        if(id==R.id.familyMembersText) {
            Intent i = new Intent(getApplicationContext(), FamilyMembers.class);
            startActivity(i);
        }

        if(id==R.id.numbersText) {
            Intent i = new Intent(getApplicationContext(), Numbers.class);
            startActivity(i);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(MainActivity.this, "Settings coming soon :)", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
