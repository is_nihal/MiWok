package com.lmntrx.android.nihal.miwok;

/**
 * Created by nihal on 12-Jul-16.
 */
public class Word {

    private String englishWord,miWokWord,type;

    private int imageId;

    public Word(String englishInput,String miWokInput,int imageInput,String typeInput)
    {
        englishWord=englishInput;
        miWokWord=miWokInput;
        imageId=imageInput;
        type=typeInput;
    }

    public String getEnglishWord()
    {
        return englishWord;
    }

    public String getMiWokWord()
    {
        return miWokWord;
    }

    public int getImageId()
    {
        return imageId;
    }

    public String getType()
    {
        return type;
    }
}
