package com.lmntrx.android.nihal.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class FamilyMembers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_members);

        ListView listView=(ListView)findViewById(R.id.familyList);

        ArrayList<Word>arrayList=new ArrayList<Word>();
        arrayList.add(new Word("Father","Apa",R.drawable.family_father,"family"));
        arrayList.add(new Word("Mother","Ata",R.drawable.family_mother,"family"));
        arrayList.add(new Word("Son","Angsi",R.drawable.family_son,"family"));
        arrayList.add(new Word("Daughter","Tune",R.drawable.family_daughter,"family"));
        arrayList.add(new Word("Older Brother","Taachi",R.drawable.family_older_brother,"family"));
        arrayList.add(new Word("Younger Brother","Chalitti",R.drawable.family_younger_brother,"family"));
        arrayList.add(new Word("Older Sister","Tete",R.drawable.family_older_sister,"family"));
        arrayList.add(new Word("Younger Sister","Kolliti",R.drawable.family_younger_sister,"family"));
        arrayList.add(new Word("Grandmother","Ama",R.drawable.family_grandmother,"family"));
        arrayList.add(new Word("Grandfather","Paapa",R.drawable.family_grandfather,"family"));

        WordAdapter wordAdapter=new WordAdapter(this,arrayList);

        listView.setAdapter(wordAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(position==0)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_father);
                    mediaPlayer.start();
                }

                else if(position==1)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_mother);
                    mediaPlayer.start();
                }

                else if(position==2)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_son);
                    mediaPlayer.start();
                }

                else if(position==3)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_daughter);
                    mediaPlayer.start();
                }

                else if(position==4)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_older_brother);
                    mediaPlayer.start();
                }

                else if(position==5)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_younger_brother);
                    mediaPlayer.start();
                }

                else if(position==6)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_older_sister);
                    mediaPlayer.start();
                }

                else if(position==7)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_younger_sister);
                    mediaPlayer.start();
                }

                else if(position==8)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_grandmother);
                    mediaPlayer.start();
                }

                else if(position==9)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.family_grandfather);
                    mediaPlayer.start();
                }
            }
        });
    }
}
