package com.lmntrx.android.nihal.miwok;

import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Colors extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);

        ListView listView=(ListView)findViewById(R.id.colorsList);

        ArrayList<Word> arrayList=new ArrayList<Word>();

        arrayList.add(new Word("Red","Wetteti",R.drawable.color_red,"color"));
        arrayList.add(new Word("Green","Chokokki",R.drawable.color_green,"color"));
        arrayList.add(new Word("Brown","Takaakki",R.drawable.color_brown,"color"));
        arrayList.add(new Word("Gray","Topoppi",R.drawable.color_gray,"color"));
        arrayList.add(new Word("Black","Kululi",R.drawable.color_black,"color"));
        arrayList.add(new Word("White","Kelelli",R.drawable.color_white,"color"));
        arrayList.add(new Word("Dusty Yellow","Topisse",R.drawable.color_dusty_yellow,"color"));
        arrayList.add(new Word("Mustard Yellow","Chiwitta",R.drawable.color_mustard_yellow,"color"));


        WordAdapter wordAdapter=new WordAdapter(this,arrayList);

        listView.setAdapter(wordAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(position==0)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_red);
                    mediaPlayer.start();
                }

                else if(position==1)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_green);
                    mediaPlayer.start();
                }

                else if(position==2)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_brown);
                    mediaPlayer.start();
                }

                else if(position==3)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_gray);
                    mediaPlayer.start();
                }

                else if(position==4)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_black);
                    mediaPlayer.start();
                }

                else if(position==5)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_white);
                    mediaPlayer.start();
                }

                else if(position==6)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_dusty_yellow);
                    mediaPlayer.start();
                }

                else if(position==7)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.color_mustard_yellow);
                    mediaPlayer.start();
                }
            }
        });
    }
}
