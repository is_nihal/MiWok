package com.lmntrx.android.nihal.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Numbers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        ListView listView=(ListView)findViewById(R.id.numbersList);

        ArrayList<Word>arrayList=new ArrayList<Word>();
        arrayList.add(new Word("One","Lutti",R.drawable.number_one,"numbers"));
        arrayList.add(new Word("Two","Ottiko",R.drawable.number_two,"numbers"));
        arrayList.add(new Word("Three","Tolookosu",R.drawable.number_three,"numbers"));
        arrayList.add(new Word("Four","Oyissa",R.drawable.number_four,"numbers"));
        arrayList.add(new Word("Five","Massoka",R.drawable.number_five,"numbers"));
        arrayList.add(new Word("Six","Temmokka",R.drawable.number_six,"numbers"));
        arrayList.add(new Word("Seven","Kenekaku",R.drawable.number_seven,"numbers"));
        arrayList.add(new Word("Eight","Kawinta",R.drawable.number_eight,"numbers"));
        arrayList.add(new Word("Nine","Wo'e",R.drawable.number_nine,"numbers"));
        arrayList.add(new Word("Ten","na'aacha",R.drawable.number_ten,"numbers"));

        WordAdapter wordAdapter=new WordAdapter(this,arrayList);

        listView.setAdapter(wordAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(position==0)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_one);
                    mediaPlayer.start();
                }

                else if(position==1)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_two);
                    mediaPlayer.start();
                }

                else if(position==2)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_three);
                    mediaPlayer.start();
                }

                else if(position==3)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_four);
                    mediaPlayer.start();
                }

                else if(position==4)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_five);
                    mediaPlayer.start();
                }

                else if(position==5)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_six);
                    mediaPlayer.start();
                }

                else if(position==6)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_seven);
                    mediaPlayer.start();
                }

                else if(position==7)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_eight);
                    mediaPlayer.start();
                }

                else if(position==8)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_nine);
                    mediaPlayer.start();
                }

                else if(position==9)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.number_ten);
                    mediaPlayer.start();
                }
            }
        });
    }
}
