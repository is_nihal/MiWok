package com.lmntrx.android.nihal.miwok;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nihal on 12-Jul-16.
 */
public class WordAdapter extends ArrayAdapter<Word>{

    public WordAdapter(Activity context, ArrayList<Word>arrayList) {
        super(context,0,arrayList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.cutstom_list, parent, false);
        }

        Word currentWord=getItem(position);

        TextView miWokText=(TextView)listItemView.findViewById(R.id.listText1);
        miWokText.setText(currentWord.getMiWokWord());

        TextView englishText=(TextView)listItemView.findViewById(R.id.listText2);
        englishText.setText(currentWord.getEnglishWord());

        ImageView imageView=(ImageView)listItemView.findViewById(R.id.listImage);
        imageView.setImageResource(currentWord.getImageId());

        if(currentWord.getType().equals("phrases"))
        {
            miWokText.setBackgroundColor(0xFF16AFCA);
            englishText.setBackgroundColor(0xFF16AFCA);
        }

        else if(currentWord.getType().equals("color"))
        {
            miWokText.setBackgroundColor(0xFF8800A0);
            englishText.setBackgroundColor(0xFF8800A0);
        }

        else if(currentWord.getType().equals("numbers"))
        {
            miWokText.setBackgroundColor(0xFFFD8E09);
            englishText.setBackgroundColor(0xFFFD8E09);
        }

        else
        {
            miWokText.setBackgroundColor(0xFF379237);
            englishText.setBackgroundColor(0xFF379237);
        }


        return listItemView;
    }
}
